LuaQ  $   @Script/Activity/TeachersDayAct.lua           
      @@ ��  ��	 ��	���	 d   	@�� � 
   	   Activity    GetUiSetting    TeachersDayAct    nShowLevel       �?   szTitle    Cùng Vui Lễ Sư Đồ 	   szUiName    Normal    FnCustomData                   �   �@@�� ����   �@�F�� L�܀�A J  �� ��A�   @�� ��bA  ^  � 	      Lib 
   TimeDesc7    nStartTime 	   nEndTime       �?�       Học trò khắp chốn, ánh xuân tứ phương, dù là tiền bối trong cuộc sống, công việc hay giang hồ, chỉ cần là người từng dạy chúng ta đều là ân sư của chúng ta. Nhân ngày đặc biệt, Quản Lý Sư Đồ Thượng Quan Phi Long đã mở phó bản [FFFE0D]【Thí Luyện Sư Đồ】[-] để thử thách sự phối hợp ăn ý của các vị sư đồ! Nhiều phần thưởng đang chờ các vị!
     Giới thiệu: Trong hoạt động, 2 người chơi có quan hệ Sư Đồ (Gồm Xuất Sư) [FFFE0D]tặng nhau 5 Hoa Hồng/Cỏ May Mắn[-] (99 Hoa Hồng/Cỏ May Mắn vô hiệu), Đồ Đệ nhận ngay [ff8f06] [url=openwnd:Tín Vật Sư Đồ, ItemTips, 'Item', nil, 6232] [-]. 2 Sư Đồ có thể tổ đội đến chỗ Thượng Quan Phi Long giao [ff8f06] [url=openwnd:Tín Vật Sư Đồ, ItemTips, 'Item', nil, 6232] [-] để tham gia Thí Luyện Sư Đồ.
     Lưu ý: 
     (1) Đội tham gia phó bản phải là đội chỉ có 2 Sư Đồ; 
     (2) Đồ Đệ cần mang Tín Vật Sư Đồ; 
     (3) Lần đầu thành công nhận nhiều thưởng lần đầu và thưởng cơ bản, các phó bản sau chỉ nhận thưởng cơ bản; 
     (4) Trong thời gian hoạt động, Đồ Đệ tối đa nhận 5 Tín Vật, Sư Phụ tối đa nhận thưởng 10 lần; 
     (5) Sư Phụ khiêu chiến 5 lần, 10 lần Thí Luyện Sư Đồ được nhận danh hiệu đặc biệt [FFFE0D]Thầy Tốt Bạn Hiền (1 tháng) [-], [FFFE0D]Bách Thế Chi Sư (1 tháng)[-], Đồ Đệ khiêu chiến 5 lần được nhận danh hiệu đặc biệt [FFFE0D]Môn Sinh Tài Ba (1 tháng)[-]; 
     (6) Trong thí luyện nếu rời phó bản hoặc chưa vượt ải trong thời gian quy định xem như khiêu chiến thất bại và không được thưởng; 
     (7) Các vị Sư Đồ hãy cùng hợp tác, thể hiện tài năng, chú ý gợi ý trong phó bản!    string    format 1   Thời gian hoạt động: [c8ff00]%s-%s[-]

%s                                   	   
   
   
   
   
   
   
   
   
   
            _           tbData           szStart          szEnd 	      
   szContent 
          
                                       tbAct    	       