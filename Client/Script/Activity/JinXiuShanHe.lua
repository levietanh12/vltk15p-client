LuaQ  "   @Script/Activity/JinXiuShanHe.lua                 @@ ��  ��	 ��	���	 	�d       	@�� �    	   Activity    GetUiSetting    JinXiuShanHe    nShowLevel       4@   szTitle    Ngày Lễ-Cẩm Tú Sơn Hà 	   szUiName    Normal 
   szContent �  [FFFE0D]Hoạt động Ngày Lễ Cẩm Tú Sơn Hà đã bắt đầu![-]

Tìm 34 Thẻ Khu Vực, dựa theo xếp hạng độ hoàn thành để nhận thưởng khi hoạt động kết thúc.
[FFFE0D]Thời gian hoạt động: [-]%s-%s
[FFFE0D]Cấp vào: [-]Lv%d

[FFFE0D]1. Thẻ Cẩm Tú Sơn Hà[-]
    Trong thời gian hoạt động nhận phần thưởng [FFFE0D]Mục Tiêu Ngày[-] hoặc mua [FFFE0D]Quà Mỗi Ngày[-] bất kỳ (Giới hạn nhận 1 tấm) sẽ nhận đạo cụ [11adf6][url=openwnd:Thẻ Cẩm Tú Sơn Hà, ItemTips, "Item", nil, 3029][-], đạo cụ này cần nhấp dùng để [FFFE0D]giám định[-].
    Sau khi giám định sẽ nhận 1 [FFFE0D]Thẻ Khu Vực[-] bất kỳ, như [11adf6][url=openwnd:Cẩm Tú Sơn Hà-Đài, ItemTips, "Item", nil, 3031][-]. Tên của Thẻ Khu Vực là viết tắt tên của các tỉnh thành.
    Thẻ Khu Vực sau khi sử dụng sẽ thêm vào Sách Thu Thập [11adf6][url=openwnd:Sách Cẩm Tú Sơn Hà, ItemTips, "Item", nil, 3065][-], thông qua [FFFE0D]độ hoàn thành[-] của Sách Thu Thập để tiến hành xếp hạng và nhận thưởng khi hoạt động kết thúc.
    Chú ý: Mỗi ngày giám định tối đa 5 Thẻ Cẩm Tú Sơn Hà.
[FFFE0D]2. Thẻ Thần Châu[-]
    Giám định Thẻ Cẩm Tú Sơn Hà có tỉ lệ nhận [11adf6][url=openwnd:Thẻ Thần Châu, ItemTips, "Item", nil, 3030][-], dùng nhận Thẻ Khu Vực mà Sách Cẩm Tú Sơn Hà [FFFE0D]chưa thu thập được[-]. Nếu đã thu thập đủ sẽ nhận Thẻ Khu Vực ngẫu nhiên.
[FFFE0D]3. Thưởng hoạt động[-]
    Hoạt động kết thúc phát thưởng theo xếp hạng, phần thưởng như sau: 
    Hạng 1: 60 Thủy Tinh Lam, Danh hiệu Cam Tri Thiên Hiểu Địa
    Hạng 2-10: 40 Thủy Tinh Lam, Danh hiệu Hồng Vạn Sự Thông
    Hạng 11-30: 35 Thủy Tinh Lam, Danh hiệu Tím Lòng Tỏ Dạ Sáng
    Hạng 31-100: 30 Thủy Tinh Lam
    Hạng 101-200: 25 Thủy Tinh Lam
    Hạng 201-300: 20 Thủy Tinh Lam
    Hạng 301-500: 16 Thủy Tinh Lam
    Hạng 501-1000: 12 Thủy Tinh Lam
    Hạng 1001-1500: 10 Thủy Tinh Lam
    Hạng 1500+: 8 Thủy Tinh Lam
    FnCustomData            $    
   �   �@@�� ����   �@�F�� ܀�
  E FA��  ��A�  �D  F��\�"A    �       Lib    TimeDesc10    nStartTime 	   nEndTime    string    format 
   szContent    nShowLevel        !   !   !   !   "   "   "   "   #   #   #   #   #   #   #   #   #   #   #   #   $         szKey           tbData           szStart          szEnd             tbAct                            $   $   $   $         tbAct           