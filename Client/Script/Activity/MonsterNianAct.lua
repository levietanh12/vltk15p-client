LuaQ  $   @Script/Activity/MonsterNianAct.lua                 @@ ��  ��	 ��	���	 	�d       	@�� �    	   Activity    GetUiSetting    MonsterNianAct    nShowLevel       $@   szTitle    Niên Thú Xuất Hiện 	   szUiName    Normal 
   szContent 7  [FFFE0D]Hoạt động năm mới Đuổi Niên Thú đã bắt đầu![-]

[FFFE0D]Thời gian hoạt động: [-]%s-%s
[FFFE0D]Cấp vào: [-]Lv20

    Niên Thú là ác thú trong truyền thuyết cổ đại. Tương truyền thời đó mỗi lần đến nửa đêm cuối năm là Niên Thú sẽ tấn công thôn trang. Sau này có người phát hiện Niên Thú sợ màu đỏ và pháo nên việc dán câu đối và đốt pháo đã trở thành tập tục năm mới.
    Trong thời gian hoạt động, khi bang đốt lửa trại, tại [FFFE0D]Bang Hội[-] sẽ xuất hiện [FFFE0D]Niên Thú[-] làm loạn, chỉ có thể thông qua [FFFE0D]Pháo Hoa[-] để gây sát thương.
    Khi sinh lực Niên Thú đạt [FFFE0D]70%%[-], [FFFE0D]40%%[-], [FFFE0D]0%%[-] sẽ xuất hiện Bảo Rương ở gần đó, mỗi ngày mỗi người chỉ được thu thập [FFFE0D]10[-] Bảo Rương.
    Sau khi hoạt động kết thúc, báu vật Niên Thú rơi ra sẽ tiến hành đấu giá tại Bang Hội, người chơi tham gia Đuổi Niên Thú có thể nhận [FFFE0D]lợi tức đấu giá[-].
    Trong thời gian hoạt động Niên Thú sẽ tạm dừng vấn đáp Bang Hội và có thể nhận EXP Lửa Trại trong [FFFE0D]tất cả bản đồ[-] thuộc Bang Hội.
    FnCustomData               	   �   �@@�� ����   �@�F�� ܀�
  E FA��  ��A�  �\ "A    �       Lib    TimeDesc10    nStartTime 	   nEndTime    string    format 
   szContent                                                                       szKey           tbData           szStart          szEnd             tbAct                                              tbAct           