LuaQ     @AttributeDescription.lua           #      @@ ��  ��A�  �� ����� ���@���@C���C��@D���D��@E���E��@F���F��@G���G��@H�	� ���  �   �����@  �� �	� ���  	�����  	� ��  	��� � (      Ui    CreateClass    AttributeDescription �  [FF69B4][url=openwnd: Quà Tặng Minh Chủ, ItemTips, "Item", nil, 6144] [-]Cách nhận[FFFE0D][-]

[C8FF00]Mỗi lần mua/nhận Quà 20,000 VNĐ[-]    [92D2FF]Tặng kèm:[-] 1 tấm
[C8FF00]Mỗi lần mua/nhận Quà 50,000 VNĐ[-]    [92D2FF]Tặng kèm: [-] 1 tấm
[C8FF00]Mỗi lần mua/nhận Quà 100,000 VNĐ[-]    [92D2FF]Tặng kèm: [-] 2 tấm
[C8FF00]Mỗi ngày nhận Quà Nguyên Bảo 7 Ngày[-]       [92D2FF]Tặng kèm: [-] 1 tấm
[C8FF00]Mỗi ngày nhận Quà Nguyên Bảo 30 Ngày[-]     [92D2FF]Tặng kèm: [-] 1 tấm

Gợi ý: 
· Mỗi cách đều có thể nhận lại
· Mỗi tuần tối đa được nhận [C8FF00]42[-] tấm    tbSpecilKeys    InDifferBattleScoreHelp �  [92D2FF]Điểm:[-] 0-5           [92D2FF]Đánh giá:[-] [FFFFFF]Tạm[-] 
[92D2FF]Điểm:[-] 6-15         [92D2FF]Đánh giá:[-] [64db00]Thường[-] 
[92D2FF]Điểm:[-] 16-31       [92D2FF]Đánh giá:[-] [11adf6]Tốt[-] 
[92D2FF]Điểm:[-] 32-47       [92D2FF]Đánh giá:[-] [aa62fc]Ưu Tú[-] 
[92D2FF]Điểm:[-] 48-95       [92D2FF]Đánh giá:[-] [ff578c]Trác Việt[-] 
[92D2FF]Điểm:[-] 96+           [92D2FF]Đánh giá:[-] [ff8f06]Hoàn Mỹ[-]

[C8FF00]Thưởng Đấu Thường: [-]
[92D2FF]Điểm:[-] 0-5           [92D2FF]Thưởng:[-] 1200 vinh dự
[92D2FF]Điểm:[-] 6-15         [92D2FF]Thưởng:[-] 1500 vinh dự
[92D2FF]Điểm:[-] 16-31       [92D2FF]Thưởng:[-] 2000 vinh dự
[92D2FF]Điểm:[-] 32-47       [92D2FF]Thưởng:[-] 2500 vinh dự
[92D2FF]Điểm:[-] 48-95       [92D2FF]Thưởng:[-] 3000 vinh dự
[92D2FF]Điểm:[-] 96+           [92D2FF]Thưởng:[-] 4000 vinh dự

Nhắc nhở: Vinh dự ảo cảnh tự đổi thành Rương Ảo Cảnh    InDifferBattleScoreHelpMonth �  [92D2FF]Điểm:[-] 0-5           [92D2FF]Đánh giá:[-] [FFFFFF]Tạm[-] 
[92D2FF]Điểm:[-] 6-15         [92D2FF]Đánh giá:[-] [64db00]Thường[-] 
[92D2FF]Điểm:[-] 16-31       [92D2FF]Đánh giá:[-] [11adf6]Tốt[-] 
[92D2FF]Điểm:[-] 32-47       [92D2FF]Đánh giá:[-] [aa62fc]Ưu Tú[-] 
[92D2FF]Điểm:[-] 48-95       [92D2FF]Đánh giá:[-] [ff578c]Trác Việt[-] 
[92D2FF]Điểm:[-] 96+           [92D2FF]Đánh giá:[-] [ff8f06]Hoàn Mỹ[-]

[C8FF00]Thưởng Đấu Tháng: [-]
[92D2FF]Điểm: [-]0-5           [92D2FF]Thưởng: [-]4000 Vinh dự
[92D2FF]Điểm: [-]6-15         [92D2FF]Thưởng: [-]6000 Vinh dự
[92D2FF]Điểm: [-]16-31       [92D2FF]Thưởng: [-]8000 Vinh dự
[92D2FF]Điểm: [-]32-47       [92D2FF]Thưởng: [-]10000 Vinh dự
[92D2FF]Điểm: [-]48-95       [92D2FF]Thưởng: [-]12000 Vinh dự
[92D2FF]Điểm: [-]96+           [92D2FF]Thưởng: [-]16000 Vinh dự

Nhắc nhở: Vinh dự ảo cảnh tự đổi thành Rương Ảo Cảnh    BeautyPageantVoteItem   [FF69B4][url=openwnd:Hồng Phấn Giai Nhân, ItemTips, "Item", nil, 4692][-]Cách nhận [FFFE0D](Chỉ trong thời gian hoạt động)[-]

[C8FF00]Mục Tiêu Ngày 100 Năng động[-]        [92D2FF]Tặng:[-]    1 đóa
[C8FF00]Mua Quà 20,000 VNĐ[-]               [92D2FF]Tặng:[-]    1 đóa
[C8FF00]Mua Quà 50,000 VNĐ[-]               [92D2FF]Tặng:[-]    2 đóa
[C8FF00]Mua Quà 100,000 VNĐ[-]               [92D2FF]Tặng:[-]    3 đóa
[C8FF00]Mua Quà Nguyên Bảo 7 Ngày[-]         [92D2FF]Tặng:[-]  18 đóa
[C8FF00]Mua Quà Nguyên Bảo 30 Ngày[-]       [92D2FF]Tặng:[-]  30 đóa
[C8FF00]Đổi Ngân Sức[-]                    [92D2FF]Tặng:[-]  34 đóa
[C8FF00]Nạp 20,000 VNĐ[-]                     [92D2FF]Tặng:[-]    2 đóa
[C8FF00]Nạp 100,000 VNĐ[-]                     [92D2FF]Tặng:[-]    10 đóa
[C8FF00]Nạp 200,000VNĐ[-]                     [92D2FF]Tặng:[-]    33 đóa
[C8FF00]Nạp 500,000 VNĐ[-]                 [92D2FF]Tặng:[-]  66 đóa
[C8FF00]Nạp 1,000,000 VNĐ[-]                 [92D2FF]Tặng:[-]110 đóa
[C8FF00]Nạp 1,500,000 VNĐ[-]                 [92D2FF]Tặng:[-]216 đóa

Gợi ý: Mỗi cách được nhận lại [FF69B4][url=openwnd:Hồng Phấn Giai Nhân, ItemTips, "Item", nil, 4692][-]     Lottery    WeddingWelcome �   Đón khách trước khi hôn lễ bắt đầu, tân lang tân nương có thể gửi Thiệp Mời cho hảo hữu, thành viên bang

[FFFE0D]Nên thông báo hảo hữu trước khi tổ chức hôn lễ[-]     WeddingPromise �   Tình yêu là bao dung, quan tâm, nếu yêu hãy can đảm bày tỏ cho cả thế giới biết!

[FFFE0D]Lời hứa của hai người sẽ được ghi trên Hôn Thư[-]     WeddingCeremony �   Thành hôn: Nhất bái thiên địa, nhị bái cao tổ, phu thê giao bái

[FFFE0D]Bái đường xong nhận danh hiệu Phu Thê, Hôn Thư, Nhẫn Cưới[-]     WeddingFirecracker p   Pháo mừng hôn lễ, tân nhân, khách mời cùng múa, xem pháo hoa và chúc phúc cho đôi tân nhân    WeddingConcentricFurit C   Phu thê đồng lòng, trong 1 giây cùng ăn Quả Đồng Tâm    WeddingTableFood :   Tiệc hôn lễ, cùng uống rượu, mừng tân nhân    WeddingCandy >   Tân lang tân nương phát Kẹo Hỉ, chia sẻ niềm vui    WeddingTourMap �   Đội Kiệu Hoa du thành Tương Dương, nhân sĩ giang hồ cùng đến mừng, theo sau Kiệu Hoa có cơ hội nhận Kẹo Hỉ

[FFFE0D]Tương Dương đổi sang trang trí hôn lễ, mở nhạc hôn lễ vui vẻ[-]     MarriageMDRewards_0 )   Đã nhận hết thưởng kỉ niệm
    MarriageMDRewards_1 �   [92D2FF]Dưới đây là thưởng kỉ niệm [FFFE0D]kết hôn 1 tháng[-]: [-]

[ff8f06][url=openwnd:Kim Đồng, ItemTips, "Item", nil, 5239], [url=openwnd:Ngọc Nữ, ItemTips, "Item", nil, 5240][-]     MarriageMDRewards_6    [92D2FF]Đây là quà kỉ niệm [FFFE0D]kết hôn 6 tháng[-]: [-]

[ff8f06][url=openwnd:Sàn Gỗ Lim, ItemTips, "Item", nil, 6825][-], [ff8f06][url=openwnd:Tường Thọ Văn Lưu Kim, ItemTips, "Item", nil, 6826][-], [aa62fc][url=openwnd:Lì Xì, ItemTips, "Item", nil, 6824][-]     tbSpecilFuncs    TS_CustomTaskRewards    TS_CustomTaskRewardsNone    GetText    OnOpen    OnScreenClick                #   � � �@    ��@  ƀ� �@    ��@  �� A    �A  F� ZA    �AA  �A� �A    ��A  �� � B@��B ��B��B�  �@ � ��    �       nTeacherRewards            nStudentRewards    nCurTeacherRewards    nCurStudentRewards    nCurFinished %  Hoàn thành %d nhiệm vụ sư phụ giao, đôi bên nhận thưởng
[92D2FF]Thưởng sư phụ: [-][FFFE0D]%d danh vọng[-]
[92D2FF]Thưởng đồ đệ: [-][ff4cfd]%d EXP[-]

Đồ đệ hoàn thành %d nhiệm vụ, dự tính được nhận thưởng
[92D2FF]Thưởng sư phụ: [-][FFFE0D]%d danh vọng[-]
[92D2FF]Thưởng đồ đệ: [-][ff4cfd]%d EXP[-]

Nhắc nhở:
·Thưởng EXP Đồ Đệ tăng theo cấp
·Phần thưởng sư phụ mỗi tuần tính bằng số nhiệm vụ hoàn thành tối đa của 2 đồ đệ    string    format    TeacherStudent    Def    nCustomTaskCount     #                                                                                                                           #     
   � � �@    ��@  ƀ� �@    ��@  �  E FA�� Ł �����  @�]�^   � 	      nTeacherRewards            nStudentRewards g  Hoàn thành %d nhiệm vụ sư phụ giao, đôi bên nhận thưởng
[92D2FF]Thưởng sư phụ: [-][FFFE0D]%d danh vọng[-]
[92D2FF]Thưởng đồ đệ: [-][ff4cfd]%d EXP[-]

Nhắc nhở:
·Thưởng EXP Đồ Đệ tăng theo cấp
·Phần thưởng sư phụ mỗi tuần tính bằng số nhiệm vụ hoàn thành tối đa của 2 đồ đệ    string    format    TeacherStudent    Def    nCustomTaskCount                                    !   "   "   "   "   "   "   "   "   "   "   #               %   *        � @ �@��   @�� @ �@�   @ � ��   �@@ �@��   �       tbSpecilFuncs    tbSpecilKeys        &   &   &   &   '   '   '   '   '   '   )   )   )   *               +   2     
   K@ �A�  ��A    \� Z    �@ ���@ ��@ �   � �B� ZB    �B  �A �AA ��A � �A� �       GetText        pPanel 
   SetActive    Arrow    Description    SetLinkText        ,   ,   ,   ,   ,   ,   -   -   .   0   0   0   0   0   0   0   0   0   0   1   1   1   1   2               3   5        �   �@@�@ �@� �       Ui    CloseWindow    UI_NAME        4   4   4   4   5           #                                    	   
                                       #   #   $   *   %   2   +   5   3   5                   NTX]|p8����)�|hMI=I�lnmlsT�1>}L)�|hMJ=I�lnmlmd9�=,M|�LF}d,y�\�UJ��"  � ߸�35'3��1%���I�����K�'�/�?lu���'2cH��b۲���6:�OC!�l�o���J