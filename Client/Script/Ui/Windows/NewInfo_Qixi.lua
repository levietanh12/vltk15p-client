LuaQ  $   @Script/Ui/Windows/NewInfo_Qixi.lua                 @@ ��  ��	 ��d   	@��d@  	@ �J@  ��  I� �	@�� � 	      Ui    CreateClass    NewInfo_Qixi 
   szContent �  [FFFE0D]Sự kiện Lễ Thất Tịch đã bắt đầu![-]
Lễ Thất Tịch bắt nguồn từ thời Hán, sau này trở thành ngày lễ tình yêu qua truyền thuyết “Ngưu Lang Chức Nữ”.

[FFFE0D]Thời gian[-]: %s-%s
[FFFE0D]Điều kiện[-]: Đạt [FFFE0D]Lv%d[-]

[FFFE0D]Giới thiệu[-]: 
1. Hoa Tửu Thi Kiếm
Nhận thưởng [FFFE0D]Mục Tiêu Ngày[-] hoặc [FFFE0D]Cống Hiến Bang Hội[-], nhận được Thất Lăng Hoa, Thất Vị Tửu, Thơ Thất Ngôn và Thất Thốn Kiếm, ghép lại nhận được Hoa Tửu và Thi Kiếm.
Tổ đội hảo hữu [FFFE0D]nam nữ[-] thân mật trên [FFFE0D]Lv%d[-], có thể tặng quà tại chỗ, đối phương nhận được [FFFE0D]「Quà」Hoa Tửu[-] hoặc [FFFE0D]「Quà」Thi Kiếm[-] , trong quá trình đó được thưởng thêm [FFFE0D]thân mật[-].

    [FFFE0D]Chú ý[-]: 
Cống Hiến Bang Hội nhận tối đa [FFFE0D]5[-] đạo cụ hoạt động.
Không hạn chế số lần tặng và nhận quà.
Hoạt động kết thúc, có thể ghép và tặng đạo cụ, nhưng không được thưởng thân mật.

2. Tế Sao Huyền Hương
 Có [FFFE0D]Hoa Tửu[-] và [FFFE0D]Thi Kiếm[-] ([FFFE0D]Quà tặng[-] cũng được), có thể đến [FFFE0D]Tương Dương[-] tìm [FFFE0D]Nạp Lan Chân[-] nhận [FFFE0D]Thất Huyền Hương[-]. [FFFE0D]Tổ đội[-] hảo hữu nam nữ thân mật trên [FFFE0D]Lv%d[-], sử dụng Thất Huyền Hương, cùng Tế Sao tại địa điểm ngẫu nhiên, nhận nhiều thưởng và EXP.

Mỗi ngày được Tế Sao Huyền Hương [FFFE0D]%d lần[-], số lần được [FFFE0D]tích lũy[-] trong thời gian diễn ra hoạt động.
Mỗi ngày có thể [FFFE0D]giúp[-] người khác Tế Sao Huyền Hương [FFFE0D]%d lần[-], số lần được [FFFE0D]tích lũy[-] trong thời gian diễn ra hoạt động.
Hoạt động kết thúc, Thất Huyền Hương không thể sử dụng.
    OnOpen    GetMsgContent 
   tbOnClick 
   BtnNormal           0     T   �   �@@�� ����   ���@ ���E KA�ƁA\��� �AA�A����  ���A�� �BFC ��� C�F�����������FBD K��� \B�F�D K��B \����D �BE� �����D ���A� �F�C�����B���D ���A� �B���D ��A� �B��BG �B    ���  	���ł ܂� CG ��X��� �� ��܂  H�CG ��W �@��B Ƃ��B� ł ܂� 	��� � #   	   Activity    GetActKeyName       �?   GetActUiSetting    Lib    TimeDesc10    nStartTime 	   nEndTime    Qixi    Def    string    format 
   szContent    OPEN_LEVEL    IMITYLEVEL    CHANGE_ITEM_TIMES    HELP_AWARD_TIMES 	   Content2    SetLinkText    pPanel    Label_GetPrintSize    Widget_GetSize    datagroup2    Widget_SetSize    x    y       I@   DragScrollViewGoTop    UpdateDragScrollView    nLastOpenTime    GetTime       N@   GetLocalDay    RemoteServer    TryUpdateQixiData     T                                                       !   !   !   "   "   "   "   "   "   "   "   "   "   "   #   #   #   #   %   %   %   %   &   &   &   &   '   '   '   '   '   '   '   (   (   (   (   )   )   )   )   +   +   +   +   +   ,   ,   ,   ,   ,   ,   ,   ,   ,   ,   ,   ,   ,   ,   ,   -   -   -   .   .   .   0         self     S      tbData     S   	   szActKey    S      _    S   
   tbActData    S      szStartTime    S   
   szEndTime    S      tbDef    S   
   szContent    S      tbTextSize &   S      tbSize *   S           2   ;     %   A   �@  ��@��@�  �@�܀  AA��A��� ����BEA F��� ��B\���A ��B� C����A Ɓ� � @���C���� D�܁ @ �^   �    ]   Số lần Huyền Hương Tế Sao còn: %d/%d
Số lần [FFFE0D]hỗ trợ[-] còn: %d/%d 	   Activity    Qixi    Def    Lib    GetLocalDay    ACTIVITY_TIME_BEGIN       �?   SAVE_GROUP    me    GetUserValue    CHANGE_ITEM_TIMES_KEY    HELP_AWARD_TIMES_KEY    string    format    CHANGE_ITEM_TIMES    HELP_AWARD_TIMES     %   3   4   4   4   5   5   5   5   5   5   5   5   5   6   7   7   7   7   7   8   8   8   8   8   9   9   9   9   9   9   9   9   9   9   9   :   ;         self     $      szMsg    $      Def    $   	   nOpenDay    $      nGroup    $   	   nBaixing    $      nHelp    $           >   A        K @ \� �@  ��@�  @� �� �� $  �A� �A� ��  �A� ��� �@� �       GetMsgContent    Ui    OpenWindow    MessageBox    Đồng ý        @   @          �            @              ?   ?   @   @   @   @   @   @   @   @   @   @   @   @   @   @   @   A         self           szMsg                             0      ;   2   =   A   A   B   B         tbUi           