LuaQ  +   @Script/Ui/Windows/CardPickRecordPanel.lua                 @@ ��  ��A�  �   	� ��@  	�����    � 	� ���  	���� B �@    ��   	� �� B �  ���� � 
      Ui    CreateClass    CardPickRecordPanel �  
[73cbd5]1)Chiêu mộ Đồng Hành-Bạc mua[-]
Tỉ lệ chiêu mộ Đồng Hành [ff42c7]Cấp A[-]: [c8ff00]5%[-]
Tỉ lệ chiêu mộ Đồng Hành [42ccff]Cấp B[-]: [c8ff00]27.5%[-]
Tỉ lệ chiêu mộ Đồng Hành [42ff58]Cấp C[-]: [c8ff00]67.5%[-]

[73cbd5]2) Chiêu mộ Đồng Hành-Mua Nguyên Bảo[-]
Tỉ lệ chiêu mộ Đồng Hành [ff5f63]Cấp S[-]: [c8ff00]1%[-]
Tỉ lệ chiêu mộ Đồng Hành [ff42c7]Cấp A[-]: [c8ff00]12.5%[-]
Tỉ lệ chiêu mộ Đồng Hành [42ccff]Cấp B[-]: [c8ff00]30.61%[-]
Tỉ lệ chiêu mộ Tẩy Tủy Đơn: [c8ff00]55.89%[-]
[73cbd5]Ngoài ra trò chơi còn thiết lập cứ 10 lần chiêu mộ sẽ có Đồng Hành Cấp S trở lên, tỉ lệ chiêu như sau:[-]
Tỉ lệ chiêu mộ Đồng Hành [ffa23e]Cấp SS[-]: [c8ff00]8%[-]
Tỉ lệ chiêu mộ Đồng Hành [ff5f63]Cấp S[-]: [c8ff00]92%[-]

[73cbd5]3) Rung Cây Tiền[-]
Tỉ lệ Rung Tiền Thường: [c8ff00]72%[-]
Tỉ lệ bạo kích x2: [c8ff00]24%[-]
Tỉ lệ bạo kích x10: [c8ff00]4%[-]

[73cbd5]4) Rương Hoàng Kim[-]
Tỉ lệ mở được Bạc: [c8ff00]20%[-]
Tỉ lệ mở được Trang Bị Kế Thừa Bậc 2: [c8ff00]20%[-]
Tỉ lệ mở được Trang Bị Kế Thừa Bậc 3: [c8ff00]10%[-]
Tỉ lệ mở được Mảnh Trang Bị Kế Thừa Bậc 4: [c8ff00]24%[-]
Tỉ lệ mở được Mảnh Trang Bị Kế Thừa Bậc 5: [c8ff00]12%[-]
Tỉ lệ mở được Mảnh Trang Bị Kế Thừa Bậc 6: [c8ff00]10%[-]
Tỉ lệ mở được Mảnh Trang Bị Kế Thừa Bậc 7: [c8ff00]6%[-]
Tỉ lệ mở được Mảnh Trang Bị Kế Thừa Bậc 8: [c8ff00]4.8%[-]
Tỉ lệ mở được Mảnh Trang Bị Kế Thừa Bậc 9: [c8ff00]4.2%[-]
Tỉ lệ mở được Mảnh Trang Bị Kế Thừa Bậc 10: [c8ff00]3%[-]
Tỉ lệ mở được Đá Hồn Lv1: [c8ff00]10%[-]
Tỉ lệ mở được Đá Hồn Lv2: [c8ff00]2%[-]
Tỉ lệ mở được Đá Hồn Lv3: [c8ff00]0.6%[-]
Tỉ lệ mở được Đá Hồn Lv4: [c8ff00]0.1%[-]

[73cbd5]5) Quà Mỗi Ngày-Quà Siêu Giá Trị 5,000 VNĐ[-]
Chắc chắn nhận Thủy Tinh Trắng x6, 20 Nguyên Bảo, 2000 Bạc, Rương Hoàng Kim.
Tỉ lệ mở được Lệnh Bài Quân Hàm: [c8ff00]0.2%[-]
Tỉ lệ mở được Trang Bị Hiếm: [c8ff00]0.5%[-]
Tỉ lệ mở được Đá Hồn Ngẫu Nhiên: [c8ff00]0.8%[-]

[73cbd5]6) Quà Mỗi Ngày-Quà Siêu Giá Trị 10,000 VNĐ[-]
[73cbd5]Trước khi mở Lv69:[-]
Chắc chắn nhận Thủy Tinh Trắng x6, Đá Hồn Lv1 Ngẫu Nhiên x1, Tẩy Tủy Đơn x1, 30 Nguyên Bảo, 200 Cống Hiến
Tỉ lệ mở được Lệnh Bài Quân Hàm: [c8ff00]0.2%[-]
Tỉ lệ mở được Trang Bị Hiếm: [c8ff00]0.5%[-]
Tỉ lệ mở được Đá Hồn Ngẫu Nhiên: [c8ff00]0.8%[-]
[73cbd5]Sau khi mở Lv69:[-]
Chắc chắn nhận Thủy Tinh Lục x2, Đạo Cụ Nguyên Khí x1, 30 Nguyên Bảo, 200 Cống Hiến
Tỉ lệ mở được Lệnh Bài Quân Hàm: [c8ff00]0.1%[-]
Tỉ lệ mở được Trang Bị Hiếm: [c8ff00]0.1%[-]
Tỉ lệ mở được Đá Hồn Ngẫu Nhiên: [c8ff00]0.15%[-]

[73cbd5]7) Quà Mỗi Ngày-Quà Siêu Giá Trị 20,000 VNĐ[-]
[73cbd5]Trước khi mở Lv79:[-]
Chắc chắn nhận Đá Hồn Lv2 Ngẫu Nhiên x1, 80 Nguyên Bảo, 500 Cống Hiến.
Tỉ lệ mở được Lệnh Bài Quân Hàm: [c8ff00]0.2%[-]
Tỉ lệ mở được Trang Bị Hiếm: [c8ff00]0.5%[-]
Tỉ lệ mở được Đá Hồn Ngẫu Nhiên: [c8ff00]0.8%[-]
[73cbd5]Mở Lv79 đến trước khi mở Lv99:[-]
Chắc chắn nhận Đạo Cụ Nguyên Khí x1, Thủy Tinh Lam x1, 60 Nguyên Bảo, 400 Cống Hiến.
Tỉ lệ mở được Lệnh Bài Quân Hàm: [c8ff00]0.6%[-]
Tỉ lệ mở được Trang Bị Hiếm: [c8ff00]0.08%[-]
Tỉ lệ mở được Đá Hồn Ngẫu Nhiên: [c8ff00]1.2%[-]
[73cbd5]Sau khi mở Lv99:[-]
Chắc chắn nhận Đạo Cụ Nguyên Khí x1, Thủy Tinh Lam x1, 80 Nguyên Bảo, Quyển Nhiệm Vụ x1, Rương Hoàng Kim x2.
Tỉ lệ mở được Lệnh Bài Quân Hàm: [c8ff00]0.2%[-]
Tỉ lệ mở được Trang Bị Hiếm: [c8ff00]0.01%[-]
Tỉ lệ mở được Đá Hồn Ngẫu Nhiên: [c8ff00]0.6%[-]

    OnOpen 
   OnOpenEnd    Update    RegisterEvent 
   tbOnClick 	   BtnClose        M   Q         � � ��@  ��@�@  �       History    CardPicker    Ask4CardPickHistory        N   N   O   O   O   Q         self           szType                S   V        	@ ��@@ �@  �       szType    Update        T   U   U   V         self           szType                X   i    =   F @ W@� ��F�@ K�� �  A \@ F�@ K�� ��   \@ F�@ K�� ��  \@ @�E@ K�� \� ��@ ��@ A� �@ ��@ ��@� @� �@ ��@ ��@� A �@ F�@ K@� �� \����@ ��C� ���ƀ@ � �A� �ADƁ� ����@�ƀ@ � �A� �@�ƀ@ �@�A� �@� �       szType    History    pPanel    Label_SetText    Title $   Thông báo tỉ lệ ngẫu nhiên    Content    Tip        CardPicker    GetLatestPickHistory    Nhật ký chiêu mộ ;   Nhật ký chiêu mộ chỉ hiện 50 dòng gần nhất.    Label_GetPrintSize    Widget_GetSize 
   datagroup    Widget_SetSize    x    y       I@   DragScrollViewGoTop    UpdateDragScrollView     =   Y   Y   Y   Z   Z   Z   Z   Z   [   [   [   [   [   \   \   \   \   \   \   ^   ^   ^   _   _   _   _   _   `   `   `   `   `   a   a   a   a   a   d   d   d   d   e   e   e   e   f   f   f   f   f   f   f   g   g   g   g   h   h   h   h   i         self     <   
   szHistory    %      tbTextSize )   <      tbSize -   <         szCardPickProbTip     l   r     	   J � �  �   �@��@ �@ b@� ^   �    	   UiNotify    emNOTIFY_CARD_PICKING    Update     	   m   n   o   o   o   o   p   q   r         self           tbRegEvent               v   x        E   K@� ƀ@ \@� �       Ui    CloseWindow    UI_NAME        w   w   w   w   x         self                           K   Q   M   V   S   i   i   X   r   l   t   t   t   t   t   v   x   v   x         tbUi          szCardPickProbTip           