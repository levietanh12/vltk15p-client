LuaQ  ,   @CommonScript/SendBless/SendBlessCommon.lua           �      	���   	 ��   	���   	���   	@B�   	�B�   	@C�   	�C�   	@D�   J�  �� �@E���E��@F���F���F���ǎ��Ə�@H���F���Ƒ�@I��@I�� ʓ��ʔ� ˕��˖��Ɨ�@L���L�I����� �@M���M���M�� N��@N���Ǝ��Ǐ��N�� � 
�A� � �� "A��@� �� ��@�@O��@O���ϓ��ϔ� Е�@Ж��З�@O���P�I� �	@ �   J@ I�ĢI M�I ңI�ҤI ��	@ �   J ��  �  � �@ �  A A �@ 
 A � "A J ��  �� bA � �A  �A b@�	@��   J ���  ��D�� � AA �� �@�����ʀ  �@B�
�A �� �� "A�� ��
�  	AD�J�� � � bA�	A��b@�	@��   J ���  � A�� �� A� � �@�����ʀ  �@B�
 A �A "A � ��
�  	�@�J��� ��  bA�	A��J�  IAD�� ��  �A I�����  �AW��  A� �A ����ʁ  ��W�
�A� � �� "B����
�  	BX�J��� �� � bB�	B��J�  I�X�����  A� �B�I�����  �BY�� � A� �B ��ʂ  ��Y�
 A � "C ���
�  	CZ�J��� �� � bC�	C��b@�	@��   	 ۵   	�Ķ   	 M�   	 ҷ   	�R�   d   	@��   d@  	@ �   d�  	@��   d�  	@ �   d  	@��   d@ 	@ �   d� 	@�� � x   
   SendBless 
   nMinLevel       4@
   nStackMax       @
   nTimeStep      ��@   nMAX_SEND_TIMES    TOP_NUM       $@
   COST_GOLD       V@   nItemDelayDelteTime            szDefaultWord    Năm Mới Vui Vẻ    szWordMaxLen       >@   tbActSetting       �?
   szActName    SendBlessAct    szNotifyUi    Lantern 	   szOpenUi    BlessPanel    szNormalSprite     szGoldSprite    bRank    bGoldSkipTimes    nCardItemId      ��@   tbGetBlessAward    nMaxGetBlessAwardTimes    szItemUseName    Chúc Phúc Ngày Lễ    szMailTitle    szMailText   Ngày lễ hãy tặng Chúc Phúc cho võ lâm đồng đạo.【[FFFE0D]Thư Chúc Phúc Ngày Lễ[-]】có thể giúp ngươi thu thập và tặng chúc phúc, hãy nhận. Nội dung chi tiết xem [url=openwnd:Tin-mới, NewInformationPanel, 'SendBlessAct'].    szGetBlessMsgNormal 8   Nhận Chúc Phúc Ngày Lễ của hảo hữu「%s」    szGetBlessMsgGold F   Nhận Chúc Phúc Ngày Lễ đặc biệt của hảo hữu「%s」    szSendBlessMsg 8   Bạn tặng hảo hữu「%s」Chúc Phúc Ngày Lễ!    szColorSendMsg    szNewsTitle "   Ngày Lễ-Chúc Phúc Ngày Lễ    szNewsText �          [FFFE0D]Hoạt động Chúc Phúc ngày lễ【Chúc Phúc Ngày Lễ】bắt đầu[-]

            Dùng đạo cụ Chúc Phúc tặng Chúc Phúc cho hảo hữu, được nhận thưởng theo điểm Chúc Phúc nhận được, tham gia xếp hạng nhận danh hiệu vĩnh viễn. Hoạt động mở [FFFE0D]3 lượt[-], bắt đầu vào 0 giờ các ngày [FFFE0D]1, 4, 7[-] tháng 10, kết thúc vào [FFFE0D]4:00[-] ngày hôm sau.
        [FFFE0D]Thời gian hoạt động lượt này: [-]%s
        [FFFE0D]Cấp tham gia: [-]Lv20

        [FFFE0D]1, Thư Chúc Phúc[-]
            Mở hoạt động, người chơi đủ điều kiện nhận được 1 thư hệ thống, nhận thư xong được nhận [11adf6][url=openwnd:Thư-Chúc-Phúc-Ngày-Lễ, ItemTips, "Item", nil, 3066][-].
            Vào giao diện Chúc Phúc tặng Chúc Phúc cho hảo hữu, đối phương được nhận [FFFE0D]Điểm chúc phúc[-]. Căn cứ vào các điều kiện (Quân hàm của bên Chúc Phúc, độ thân mật hai người, quan hệ Bang Hội và Sư Đồ) được nhận [FFFE0D]Điểm chúc phúc cộng thêm[-], khi gửi [FFFE0D]Chúc Phúc Nguyên Bảo[-] cũng được cộng thêm Điểm chúc phúc.
            Thư Chúc Phúc cộng từ Top[FFFE0D]10[-] hảo hữu nhận chúc phúc, sẽ là bản thân được nhận [FFFE0D]tổng điểm chúc phúc[-].
            Mỗi lượt bắt đầu sẽ xóa hết điểm chúc phúc và tái lập trạng thái Chúc Phúc.
        [FFFE0D]2, Số lần Chúc Phúc[-]
            Tặng Chúc Phúc cho hảo hữu cần tiêu hao số lần Chúc Phúc, mỗi [FFFE0D]20[-] phút được nhận [FFFE0D]1[-] lần Chúc Phúc, tối đa là [FFFE0D]5[-].
            [FFFE0D]Mỗi lượt[-] mỗi hiệp khách tối đa  Chúc Phúc [FFFE0D]20[-] hảo hữu, chỉ được chúc phúc 1 lần với 1 người.
        [FFFE0D]3, Thưởng hoạt động[-]
            Căn cứ tổng điểm Chúc Phúc của Thư Chúc Phúc càng lớn thì được nhận [FFFE0D]thưởng càng nhiều[-], ví dụ:
            -Đạt 5: Được nhận 2 [11adf6][url=openwnd:Rương-Hoàng-Kim, ItemTips, "Item", nil, 786][-]
            -Đạt 10: Được nhận 1000 Cống hiến
            -Đạt 20: Được nhận 2 [11adf6][url=openwnd:Thủy-Tinh-Lam, ItemTips, "Item", nil, 223][-]
            -Đạt 30: Được nhận 200 Nguyên Bảo
            -Đạt 40: Được nhận 3000 Cống hiến
            -Đạt 50:  Được nhận [11adf6][url=openwnd:Rương-Đá-Hồn Lv3, ItemTips, "Item", nil, 2164][-]
            -Đạt 60:  Được nhận [11adf6][url=openwnd:Tàng-Bảo-Đồ-Cao, ItemTips, "Item", nil, 788][-]
            -Đạt 70:  Được nhận [11adf6][url=openwnd:Thủy-Tinh-Tím, ItemTips, "Item", nil, 224][-]
            -Đạt 80:  Được nhận 500 Nguyên Bảo
            -Đạt 90:  Được nhận 5000 cống hiến
            -Đạt 100: Được nhận [11adf6][url=openwnd:Rương-Đá-Hồn-Lv4, ItemTips, "Item", nil, 2165][-]

        [FFFE0D]Mỗi lượt[-] sẽ tiến hành xếp hạng người chơi theo tổng điểm Chúc Phúc trong【Bảng xếp hạng】, kết thúc được nhận [FFFE0D]Danh hiệu vĩnh viễn[-]: 
            -Hạng 1:     Danh hiệu Cam【Hảo Hữu Vô Số】
            -Hạng 2-10: Danh hiệu Hồng【Khách Quý Đông Đúc】
            -Hạng 11-30: Danh hiệu Tím【Đông Như Trẩy Hội】
                @   SendBlessActWord    NewYear 
   NewYear01 
   NewYear02    BlessPanelWord      ά@   item       �@   Chúc Phúc Năm Mới �  Đại hiệp thân mến, đường giang hồ thênh thang, ắt hẳn đại hiệp đã tìm được không ít đồng đạo võ lâm? Nhân dịp năm mới, hãy cùng gửi đến họ lời chúc phúc chân thành, cùng đón mừng xuân mới. [FFFE0D]Thư Chúc Phúc Năm Mới[-] này có thể giúp thu thập và tặng chúc phúc, xin nhận. Nội dung chi tiết xem tại [url=openwnd:Tin-Mới, NewInformationPanel, 'SendBlessActWord']. B   Chúc mừng nhận được chúc phúc năm mới từ「%s」! N   Bất ngờ nhận được chúc phúc năm mới đặc biệt từ「%s」 <   Đã tặng chúc phúc năm mới cho hảo hữu「%s」! X   Đại hiệp「%s」đã tặng chúc phúc năm mới cho bạn mình là「%s」: %s :          [FFFE0D]Lượt hoạt động chúc phúc mới nhất đã bắt đầu![-]

        Dùng đạo cụ chúc phúc có thể tặng chúc phúc cho hảo hữu nhận thưởng. Hoạt động chia làm [FFFE0D]3 lượt[-], lần lượt bắt đầu lúc 0 giờ [FFFE0D]28/1, 1/2 và 5/2[-], [FFFE0D]4 giờ hôm sau[-] kết thúc.
        [FFFE0D]Thời gian hoạt động lượt này: [-]%s
        [FFFE0D]Cấp tham gia: [-]Lv20

            Sau khi mở hoạt động, người chơi đạt đủ điều kiện sẽ nhận được thư hệ thống, mở đính kèm có thể nhận [11adf6][url=openwnd:Thư-Chúc-Phúc-Năm-Mới, ItemTips, "Item", nil, 3687][-].
            Sau khi dùng Thư Chúc Phúc, có thể tặng chúc phúc cho hảo hữu từ giao diện chúc phúc, đồng thời đối phương sẽ nhận được [FFFE0D]Rương Chúc Phúc[-]. Mỗi lượt hoạt động, mỗi người được nhận tối đa [FFFE0D]10[-] Rương Chúc Phúc.
            Mỗi lượt hoạt động, mỗi người được tặng chúc phúc cho tối đa [FFFE0D]20[-] hảo hữu, mỗi người được chúc phúc tối đa 1 lần. Tốn Nguyên Bảo sẽ không tốn số lần chúc phúc.
            Có thể nhận thưởng: [11adf6][url=openwnd:Tu Luyện Đơn, ItemTips, "Item", nil, 2301][-], [11adf6][url=openwnd:Truyền Công Đơn, ItemTips, "Item", nil, 2759][-], [11adf6][url=openwnd:Bình-Nhuộm-Ngoại-Trang, ItemTips, "Item", nil, 2569][-], [11adf6][url=openwnd:Thủy-Tinh-Tím, ItemTips, "Item", nil, 224][-].
            tbHonorScore       @      @       @      @      "@      @   tbImityScore       .@   tbRankAward 	   nRankEnd    tbAward    AddTimeTitle      �y@      �     py@     `y@   tbTakeAwardSet    nScore      ��@   Contrib      @�@     �k@   Gold       i@      D@     p�@      I@     �@      N@     ��@     �Q@      l@      T@     @@     �V@     ��@      Y@     �@   SAVE_GROUP       ]@   KEY_RESET_DAY    KEY_SEND_TIME    KEY_CUR_SEND_TIMES    KEY_TakeAwardLevel    GetNowMaxSendTimes    GetScoreInfo    GetSendTimes    CheckSendCondition    GetSendBlessVal    GetCurAwardLevel    GetActSetting        �   �        � � �@@ �@ ����@@ �� A �  �@ ܀� ̀�� FA@ ��A ��E� F��� �ABƁB ����� ���A \����^� �       GetUserValue    SAVE_GROUP    KEY_SEND_TIME         
   nStackMax    GetTime    KEY_CUR_SEND_TIMES    math    min    floor 
   nTimeStep        �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �         self           pPlayer           nLastSendTime       
   nTimeDiff          nNowSaveCur               �   �     $   Z@  @ ��   �  �   �@   � �  ��  �@@ ��B��   �ŀ  � �  d  �@��   A F�A �A  ���   ��� �  �  ����   �               pairs    table    insert    sort       �?   TOP_NUM        �   �        X �   ��@  � � �   �            �   �   �   �   �   �         a           b            $   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �         self     #      tbData     #      tbSort    #      (for generator)          (for state)          (for control)       	   dwRoleId 	         nVal 	      
   nTotalVal    #      (for index)    "      (for limit)    "      (for step)    "      i    !           �   �     
   �   A  @�  ��   @ �W�@  ����!�   ��   �               pairs        @      �?       �   �   �   �   �   �   �   �   �   �   �   �   �   �   	      self           tbSendData           bGoldSkipTimes           nCount          (for generator)          (for state)          (for control)          k    
      v    
           �   �     
E   F� �A@ ��� �F�� ��  \A  � F��Z  � �F�� � \A  � KAA \� ����  @ �A  ����� �A   ��B  �F���� I������ �AB ������� Ł ��� FBB ���A   � �A��  @��� ��C � ��� D� �Ɓ� B �A  � �� ��D�����A  � �Ɓ� B �A  � �� � �� �       nLevel 
   nMinLevel 
   CenterMsg    Cấp chưa đạt    Đã tặng    GetActSetting    bGoldSkipTimes    nSendBlessTimes    GetSendTimes    nMAX_SEND_TIMES    string    format &   Mỗi ngày tối đa tặng %d lần    bRank 
   SendBless    GetNowMaxSendTimes         3   Số lần chúc phúc được dùng không đủ 	   Activity    __IsActInProcessByType 
   szActName +   Hoạt động vòng này đã kết thúc    nCurHasCount     E   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �         self     D      pPlayer     D   
   dwRoleId2     D      tbData     D   	   bUseGold     D      tbActSetting    D      nCurHasCount 1   7      bInProcess ;   D           �       3   A  �A@ �� Ɓ@�A    �^ ��  ��@� � ܁ B E� ��A \@���� ��  �@ �a�  ��L�FB���B �B�B    ��B L�����W@A ������B�  �L�� �BC �@ �� �    �L�^  �          �?   GetActSetting    bRank    FriendShip    GetFriendImityLevel            ipairs    tbImityScore        @   nHonorLevel    tbHonorScore    dwKinId    TeacherStudent    _IsConnected     3   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �              self     2   
   dwRoleId1     2   
   dwRoleId2     2      pRole1     2      pRole2     2      nScore    2      tbActSetting    2      nFriendLevel    2   
   nAddScore    2      (for generator)          (for state)          (for control)          i          v          nSenderHonorLevel    2                 	   � � A@ F�@ ܀���FA F�ZA    � � �A ��A  �������  � � � ��� � 	      GetUserValue    SAVE_GROUP    KEY_TakeAwardLevel       �?   tbTakeAwardSet 
   SendBless    GetScoreInfo    nScore    tbAward                          	                                self           pPlayer           tbGet           nHasTakedLevel       
   nNewLevel          tbAwardInfo       
   nTotalVal                        F @ Z   @ �K @ \@ E@  F�� ��@ F�� ^   �       TryGetCurType 
   SendBless    tbActSetting    nType                                    self     
       �                                             	   	   
   
                                                                  A   B   C   D   E   F   G   H   I   J   K   L   L   L   L   L   L   L   L   M   N   O   P   Q   R   S   T   U   a   b   c   e   e   f   g   h   i   j   k   m   m   m   n   n   n   n   o   o   o   o   p   p   p   p   q   q   q   q   r   r   r   s   s   u   u   u   v   v   v   v   v   v   v   v   w   w   w   w   w   w   w   w   x   x   x   x   x   x   x   y   y   {   {   {   |   |   |   |   |   |   |   |   }   }   }   }   }   }   }   ~   ~   ~   ~   ~   ~   ~   ~                        �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �   �     �                         